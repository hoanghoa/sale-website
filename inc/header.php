<?php

include 'lib/session.php';
Session::init();
?>
<?php

include 'lib/database.php';
include 'helpers/format.php';

spl_autoload_register(function($class){
	include_once "classes/".$class.".php";
});


$db = new Database();
$fm = new Format();
$ct = new cart();
$us = new user();
$cs = new customer();
$cat = new category();
$product = new product();
?>

<?php
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache"); 
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 
header("Cache-Control: max-age=2592000");
?>

<!DOCTYPE HTML>
<head>
	<title>Store Website</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="css/menu.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
	<script src="js/jquerymain.js"></script>
	<script src="js/script.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script> 
	<script type="text/javascript" src="js/nav.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script> 
	<script type="text/javascript" src="js/nav-hover.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Doppio+One' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/menu2.css">
	<script type="text/javascript">
		$(document).ready(function($){
			$('#dc_mega-menu-orange').dcMegaMenu({rowItems:'4',speed:'fast',effect:'fade'});
		});
	</script>
</head>
<body style="font-family: 'Montserrat', sans-serif;">
	<div class="wrap">
		<div class="header_top">
			<div class="logo">
				<a href="index.php"><img src="images/logo1.png" alt="" /></a>
			</div>
			<div class="header_top_right">
				<div class="search_box">
					<form>
						<input style="font-family: 'Montserrat', sans-serif;" type="text" value="Tìm kiếm sản phẩm" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Tìm sản phẩm';}"><input style="font-family: 'Montserrat', sans-serif;" type="submit" value="Tìm kiếm">
					</form>
				</div>
				<div class="shopping_cart">
					<div class="cart">
						<a href="#" title="View my shopping cart" rel="nofollow">
							<span class="cart_title">Cart</span>
							<span class="no_product">

								<?php
								$check_cart = $ct->check_cart();
								if ($check_cart) {
									$sum = Session::get("sum");
									$qty = Session::get("qty");
									echo $fm->format_currency($sum).'Đ'.' '.' SL: '.$qty;

								}else {
									echo 'Empty';
								} 
								
								?>

							</span>
						</a>
					</div>
				</div>
				<?php 
				if(isset($_GET['customer_id'])){
					$customer_id = $_GET['customer_id'];
					$delCart = $ct->del_all_data_cart();
					$delCompare = $ct->del_compare($customer_id);
					Session::destroy();
				}
				?>   

				<?php 
				$login_check = Session::get('customer_login');
				if ($login_check==false) {
				//echo '<a href="login.php">Đăng nhập</a></div>'; 
				}else {
					echo '<a href="?customer_id='.Session::get('customer_id').' "> Thoát</a>'; 
				}
				?>


				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="navbar">
			<a style="font-family: 'Montserrat', sans-serif;" href="index.php">Trang chủ</a>
			<a style="font-family: 'Montserrat', sans-serif;" href="products.php">Sản phẩm</a>
			<div class="dropdown">
				<button class="dropbtn">Danh mục
					<i class="fa fa-caret-down"></i>
				</button>
				<div class="dropdown-content">
					<?php 
						$getall_category = $cat->show_category_fontend();
						if ($getall_category) {
							while ($result_allcat = $getall_category->fetch_assoc()) {					

								?>
								<a style="font-family: 'Montserrat', sans-serif;" href="productbycat.php?catName=<?php echo $result_allcat['catName'] ?>"><?php echo $result_allcat['catName'] ?></a>
								<?php 
							}
						}
						?>
				</div>
			</div>
			<?php 
				$check_cart = $ct->check_cart();
				if ($check_cart==true) {
					echo '<a style=" font-family: Montserrat, sans-serif; " href="cart.php">Giỏ hàng</a>';
				}else {
					echo '';
				}
				?>

				<?php 
				$customer_id = Session::get('customer_id'); 
				$check_order = $ct->check_order($customer_id);
				if ($check_order==true) {
					echo '<a style=" font-family: Montserrat, sans-serif; " href="orderdetails.php">Đơn hàng</a>';
				}else {
					echo '';
				}
				?>

				<?php 
				$login_check = Session::get('customer_login');
				if ($login_check==false) {
					echo '';
				}else {
					echo '<a style=" font-family: Montserrat, sans-serif; " href="profile.php">Thông tin</a>';
				}
				?>
				
				<a style="font-family: 'Montserrat', sans-serif;" href="contact.php">Liên hệ</a> 
		</div>
