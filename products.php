<?php 
include 'inc/header.php';
//include 'inc/slider.php';
?>

<div class="main">
	<div class="content">
		<div class="content_top">
			<div class="heading">
				<h3><a href="productbycat.php?catName=iPhone">iPhone</a></h3>
			</div>
			<div class="clear"></div>
		</div>
		<div class="section group">
			<?php 
			$product_featheread = $product->getproduct_ip();
			if($product_featheread){
				while ($result = $product_featheread->fetch_assoc()) {

					?>
					<div class="grid_1_of_4 images_1_of_4">
						<a href="details.php?proid=<?php echo $result['productId'] ?>"><img src="admin/uploads/<?php echo $result['image'] ?>" alt="" /></a>
						<h2><?php echo $result['productName'] ?></h2>
						<p><?php echo $fm->textShorten($result['product_desc'], 50) ?></p>
						<p><span class="price"><?php echo $fm->format_currency($result['price'])." "."VND" ?></span></p>
						<div class="button"><span><a href="details.php?proid=<?php echo $result['productId'] ?>" class="details">Chi tiết</a></span></div>
					</div>
					<?php 
				}
			}
			?>

		</div>
		<div class="content_bottom">
			<div class="heading">
				<h3><a href="productbycat.php?catName=iPad">iPad</a></h3>
			</div>
			<div class="clear"></div>
		</div>
		<div class="section group">
			<?php 
			$product_featheread = $product->getproduct_ipa();
			if($product_featheread){
				while ($result = $product_featheread->fetch_assoc()) {

					?>
					<div class="grid_1_of_4 images_1_of_4">
						<a href="details.php?proid=<?php echo $result['productId'] ?>"><img src="admin/uploads/<?php echo $result['image'] ?>" alt="" /></a>
						<h2><?php echo $result['productName'] ?></h2>
						<p><?php echo $fm->textShorten($result['product_desc'], 50) ?></p>
						<p><span class="price"><?php echo $fm->format_currency($result['price'])." "."VND" ?></span></p>
						<div class="button"><span><a href="details.php?proid=<?php echo $result['productId'] ?>" class="details">Chi tiết</a></span></div>
					</div>
					<?php 
				}
			}
			?>
			
		</div>

		<div class="content_bottom">
			<div class="heading">
				<h3><a href="productbycat.php?catName=Apple%20Watch">Apple Watch</a></h3>
			</div>
			<div class="clear"></div>
		</div>
		<div class="section group">
			<?php 
			$product_featheread = $product->getproduct_aw();
			if($product_featheread){
				while ($result = $product_featheread->fetch_assoc()) {

					?>
					<div class="grid_1_of_4 images_1_of_4">
						<a href="details.php?proid=<?php echo $result['productId'] ?>"><img src="admin/uploads/<?php echo $result['image'] ?>" alt="" /></a>
						<h2><?php echo $result['productName'] ?></h2>
						<p><?php echo $fm->textShorten($result['product_desc'], 50) ?></p>
						<p><span class="price"><?php echo $fm->format_currency($result['price'])." "."VND" ?></span></p>
						<div class="button"><span><a href="details.php?proid=<?php echo $result['productId'] ?>" class="details">Chi tiết</a></span></div>
					</div>
					<?php 
				}
			}
			?>
			
		</div>

		<div class="content_bottom">
			<div class="heading">
				<h3><a href="productbycat.php?catName=Macbook">Macbook</a></h3>
			</div>
			<div class="clear"></div>
		</div>
		<div class="section group">
			<?php 
			$product_featheread = $product->getproduct_mb();
			if($product_featheread){
				while ($result = $product_featheread->fetch_assoc()) {

					?>
					<div class="grid_1_of_4 images_1_of_4">
						<a href="details.php?proid=<?php echo $result['productId'] ?>"><img src="admin/uploads/<?php echo $result['image'] ?>" alt="" /></a>
						<h2><?php echo $result['productName'] ?></h2>
						<p><?php echo $fm->textShorten($result['product_desc'], 50) ?></p>
						<p><span class="price"><?php echo $fm->format_currency($result['price'])." "."VND" ?></span></p>
						<div class="button"><span><a href="details.php?proid=<?php echo $result['productId'] ?>" class="details">Chi tiết</a></span></div>
					</div>
					<?php 
				}
			}
			?>
			
		</div>

		<div class="content_bottom">
			<div class="heading">
				<h3><a href="productbycat.php?catName=iMac">iMac</a></h3>
			</div>
			<div class="clear"></div>
		</div>
		<div class="section group">
			<?php 
			$product_featheread = $product->getproduct_im();
			if($product_featheread){
				while ($result = $product_featheread->fetch_assoc()) {

					?>
					<div class="grid_1_of_4 images_1_of_4">
						<a href="details.php?proid=<?php echo $result['productId'] ?>"><img src="admin/uploads/<?php echo $result['image'] ?>" alt="" /></a>
						<h2><?php echo $result['productName'] ?></h2>
						<p><?php echo $fm->textShorten($result['product_desc'], 50) ?></p>
						<p><span class="price"><?php echo $fm->format_currency($result['price'])." "."VND" ?></span></p>
						<div class="button"><span><a href="details.php?proid=<?php echo $result['productId'] ?>" class="details">Chi tiết</a></span></div>
					</div>
					<?php 
				}
			}
			?>
			
		</div>

		<div class="content_bottom">
			<div class="heading">
				<h3><a href="productbycat.php?catName=Accessories">Phụ kiện</a></h3>
			</div>
			<div class="clear"></div>
		</div>
		<div class="section group">
			<?php 
			$product_featheread = $product->getproduct_pk();
			if($product_featheread){
				while ($result = $product_featheread->fetch_assoc()) {

					?>
					<div class="grid_1_of_4 images_1_of_4">
						<a href="details.php?proid=<?php echo $result['productId'] ?>"><img src="admin/uploads/<?php echo $result['image'] ?>" alt="" /></a>
						<h2><?php echo $result['productName'] ?></h2>
						<p><?php echo $fm->textShorten($result['product_desc'], 50) ?></p>
						<p><span class="price"><?php echo $fm->format_currency($result['price'])." "."VND" ?></span></p>
						<div class="button"><span><a href="details.php?proid=<?php echo $result['productId'] ?>" class="details">Chi tiết</a></span></div>
					</div>
					<?php 
				}
			}
			?>
			
		</div>

	</div>
</div>
<?php 
include 'inc/footer.php';
?>

