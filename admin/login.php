<?php 
	// gọi file adminlogin
	include '../classes/adminlogin.php';
 ?>
 <?php
 	// gọi class adminlogin
 	$class = new adminlogin(); 
 	if($_SERVER['REQUEST_METHOD'] == 'POST'){
 		// LẤY DỮ LIỆU TỪ PHƯƠNG THỨC Ở FORM POST
 		$adminUser = $_POST['adminUser'];
 		$adminPass = ($_POST['adminPass']);

 		$login_check = $class -> longin_admin($adminUser,$adminPass); // hàm check User and Pass khi submit lên

 	}
  ?>
<!DOCTYPE html>
<head>
<meta charset="utf-8">
	<script src="https://kit.fontawesome.com/8b1db1262c.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Admin login</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
</head>
<body>
<div class="container">
	<section class="login" id="content">
		<form action="login.php" method="post">
			<h2>Welcome</h2>
			<span><?php 
				if(isset($login_check)){
					echo $login_check;
				}
			 ?>  </span>
			<label for="">Username</label>
				<input class="txtp" type="text"  required="" name="adminUser"/>
			<label for="">Password</label>
				<input class="txtp" type="password"  required="" name="adminPass"/>
			<div class="reset"> 
				<p></p>
            <a href="forgot.php">Forgot Password?</a>
            </div>
			<div>
				<input class="btn" type="submit" value="Log in" />
			</div>
		</form><!-- form -->
		
	</section><!-- content -->
</div><!-- container -->
</body>
</html>