﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
        <!-- <div class="grid_10">
            <div class="box round first grid">
                <h2> Dashboard</h2>
                <div class="block">               
                  Welcome admin panel        
                </div>
            </div>
        </div> -->
        <?php  
        $connect = mysqli_connect("localhost", "root", "", "mvc_webdemo");  
        $query1 = "SELECT * from doanhthubyCategory";  
        $result = mysqli_query($connect, $query1);
        $query2 = "SELECT* FROM doanhthubyDatetime";  
        $result2 = mysqli_query($connect, $query2);
        $query3 = "SELECT* FROM tongdoanhthu";  
        $result3 = mysqli_query($connect, $query3);  
        ?>  
        <!DOCTYPE html>  
        <html>  
        <head>  
        	<!-- <title>Webslesson Tutorial | Make Simple Pie Chart by Google Chart API with PHP Mysql</title>   -->
        	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

      </head>  
      <body >  

		<div class="container-fluid" style="font-family: 'Montserrat', sans-serif; text-align: center; margin-left: 272px; margin-bottom: 2px; background-color: #fff; width: 1067px; height: 40px;">
			<span style=" position: relative; font-size: 30px;">Doanh thu: 
				<?php  
        				while($row = mysqli_fetch_array($result3))  
        				{  
        					echo "".$row["doanhthu"]."";  
        				}  
        				?> VNĐ
			</span>
		</div>
      	<div  class="grid_10" id="piechart" style="font-family: 'Montserrat', sans-serif; width: 520px; height: 496px;"></div>  
		<div  class="grid_10" id="ColumnChart" style="font-family: 'Montserrat', sans-serif; width: 520px; height: 496px;"></div>

      	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>  
        	<script type="text/javascript">  
        		google.charts.load('current', {'packages':['corechart']});  
        		google.charts.setOnLoadCallback(drawChart);  
        		function drawChart()  
        		{  
        			var data = google.visualization.arrayToDataTable([  
        				['catName', 'doanhthu'],  
        				<?php  
        				while($row = mysqli_fetch_array($result))  
        				{  
        					echo "['".$row["catName"]."', ".$row["doanhthu"]."],";  
        				}  
        				?>  
        				]);  
        			var options = {  
        				title: 'Doanh thu theo loại sản phẩm',  
                      //is3D:true,  
                      pieHole: 0.4  
                  };  
                  var chart = new google.visualization.PieChart(document.getElementById('piechart'));  
                  chart.draw(data, options);  
              }  
          </script>


		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>  
        	<script type="text/javascript">  
        		google.charts.load('current', {'packages':['corechart']});  
        		google.charts.setOnLoadCallback(drawChart);  
        		function drawChart()  
        		{  
        			var data = google.visualization.arrayToDataTable([  
        				['month_year', 'doanhthu'],  
        				<?php  
        				while($row = mysqli_fetch_array($result2))  
        				{  
        					echo "['".$row["month_year"]."', ".$row["doanhthu"]."],";  
        				}  
        				?>  
        				]);  
        			var options = {  
        				title: 'Doanh thu theo tháng'/*,  
                      //is3D:true,  
                      pieHole: 0.4  */
                  };  
                  var chart = new google.visualization.ColumnChart(document.getElementById('ColumnChart'));  
                  chart.draw(data, options);  
              }  
          </script>

		
      </body>  
      </html>




      <?php include 'inc/footer.php';?>