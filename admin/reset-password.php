<?php

$email = $_GET["email"];
$reset_token = $_GET["token"];

$connection = mysqli_connect("localhost", "root", "", "mvc_webdemo");

$sql = "SELECT * FROM tbl_admin WHERE adminEmail = '$email'";
$result = mysqli_query($connection, $sql);
if (mysqli_num_rows($result) > 0)
{
	$user = mysqli_fetch_object($result);
	if ($user->token == $reset_token)
	{
		?>
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>Reset Password</title>
			<link rel="stylesheet" href="css2/forgot.css">
		</head>
		<body>
			<div class="wrapper fadeInDown">
				<div id="formContent">
					<!-- Tabs Titles -->
					<h2 class="active"> Enter your new password </h2>

					<!-- Icon -->


					<!-- Login Form -->
					<form method="POST" action="new-password.php">
						<input type="hidden" name="email" value="<?php echo $email; ?>">
						<input type="hidden" name="reset_token" value="<?php echo $reset_token; ?>">

						<input style="background-color: #f6f6f6;
						border: none;
						color: #0d0d0d;
						padding: 15px 32px;
						text-align: center;
						text-decoration: none;
						display: inline-block;
						font-size: 16px;
						margin: 5px;
						width: 85%;
						border: 2px solid #f6f6f6;
						-webkit-transition: all 0.5s ease-in-out;
						-moz-transition: all 0.5s ease-in-out;
						-ms-transition: all 0.5s ease-in-out;
						-o-transition: all 0.5s ease-in-out;
						transition: all 0.5s ease-in-out;
						-webkit-border-radius: 5px 5px 5px 5px;
						border-radius: 5px 5px 5px 5px;" type="password" id="login" class="fadeIn second" name="new_password" placeholder="Enter new password">
						<input type="submit" class="fadeIn fourth" value="Change password">
					</form>

					<!-- Remind Passowrd -->


				</div>
			</div>
		</body>
		</html>

		<!-- <link rel="stylesheet" href="css/forgot.css">
		<form method="POST" action="new-password.php">
			<input type="hidden" name="email" value="<?php echo $email; ?>">
			<input type="hidden" name="reset_token" value="<?php echo $reset_token; ?>">
			
			<input type="password" id="login" class="fadeIn second" name="new_password" placeholder="Enter new password">
			<input type="submit" class="fadeIn fourth" value="Change password">
		</form> -->
		<?php
	}
	else
	{
		echo "Recovery email has been expired";
	}
}
else
{
	echo "Email does not exists";
}
