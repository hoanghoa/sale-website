<?php 
include 'inc/header.php';
	// include 'inc/slider.php';
?>
<?php 
if(!isset($_GET['catName']) || $_GET['catName'] == NULL){
	echo "<script> window.location = '404.php' </script>";
	
}else {
        $name = $_GET['catName']; // Lấy productid trên host
    }
    // gọi class category
    // if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //     // LẤY DỮ LIỆU TỪ PHƯƠNG THỨC Ở FORM POST
    //     $catName = $_POST['catName'];
    //     $updateCat = $cat -> update_category($catName,$id); // hàm check catName khi submit lên
    // }
    
    ?>
    <div class="main">
	<h2 style="font-size: 40px;">
		<?php echo $name; ?>
	</h2>
    	<div class="content">
    		<div class="content_top">
    			<?php 
    			$prod = $product->get_product_by_name($name);
    			if($prod){
    				while ($result = $prod->fetch_assoc()) {

    					?>
    					<div class="grid_1_of_4 images_1_of_4">
    						<a href="details.php?proid=<?php echo $result['productId'] ?>"><img src="admin/uploads/<?php echo $result['image'] ?>" alt="" /></a>
    						<h2><?php echo $result['productName'] ?></h2>
    						<p><?php echo $fm->textShorten($result['product_desc'], 50) ?></p>
    						<p><span class="price"><?php echo $fm->format_currency($result['price'])." "."VND" ?></span></p>
    						<div class="button"><span><a href="details.php?proid=<?php echo $result['productId'] ?>" class="details">Chi tiết</a></span></div>
    					</div>
    					<?php 
    				}
    			}
    			?>
    			<div class="clear"></div>
    		</div>
    		
    	</div>
    </div>

    <?php 
    include 'inc/footer.php';
    ?>